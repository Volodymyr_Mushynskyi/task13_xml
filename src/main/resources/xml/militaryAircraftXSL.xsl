<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

    <xsl:template match="/">
        <html>
            <body style="font-family: Arial; font-size: 12pt; background-color: #EEE">
                <table border="4">
                    <tr bgcolor="#2E9AFE">
                        <th>ID</th>
                        <th>Model</th>
                        <th>Origin</th>
                        <th>Type</th>
                        <th>Seater</th>
                        <th>Rockets</th>
                        <th>Radar</th>
                        <th>Length</th>
                        <th>Width</th>
                        <th>Height</th>
                        <th>Price</th>
                    </tr>

                    <xsl:for-each select="planes/plane">
                        <tr>
                            <td>
                                <xsl:value-of select="@id"/>
                            </td>
                            <td>
                                <xsl:value-of select="model"/>
                            </td>
                            <td>
                                <xsl:value-of select="origin"/>
                            </td>
                            <td>
                                <xsl:value-of select="chars/type"/>
                            </td>
                            <td>
                                <xsl:value-of select="chars/seater"/>
                            </td>
                            <td>
                                <xsl:value-of select="chars/ammunition/rockets"/>
                            </td>
                            <td>
                                <xsl:value-of select="chars/ammunition/radar"/>
                            </td>
                            <td>
                                <xsl:value-of select="parameters/length"/>
                                <xsl:text> m</xsl:text>
                            </td>
                            <td>
                                <xsl:value-of select="parameters/width"/>
                                <xsl:text> m</xsl:text>
                            </td>
                            <td>
                                <xsl:value-of select="parameters/height"/>
                                <xsl:text> m</xsl:text>
                            </td>
                            <td>
                                <xsl:value-of select="price"/>
                                <xsl:text> million</xsl:text>
                            </td>
                        </tr>
                    </xsl:for-each>
                </table>
            </body>
        </html>
    </xsl:template>
</xsl:stylesheet>