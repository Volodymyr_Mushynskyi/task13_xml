package com.epam.constant;

public class Constant {

  public static final String PLANE = "plane";
  public static final String ID = "id";
  public static final String MODEL = "model";
  public static final String ORIGIN = "origin";
  public static final String CHARS = "chars";
  public static final String TYPE = "type";
  public static final String SEATER = "seater";
  public static final String AMMUNITION = "ammunition";
  public static final String ROCKETS = "rockets";
  public static final String RADAR = "radar";
  public static final String PARAMETERS = "parameters";
  public static final String LENGTH = "length";
  public static final String WIDTH = "width";
  public static final String HEIGHT = "height";
  public static final String PRICE = "price";

  private Constant(){

  }
}
