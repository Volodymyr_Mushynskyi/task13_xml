package com.epam.compare;

import com.epam.model.Plane;

import java.util.Comparator;

public class Compare implements Comparator<Plane> {

  @Override
  public int compare(Plane o1, Plane o2) {
    return (int) (o1.getPrice() - o2.getPrice());
  }
}
