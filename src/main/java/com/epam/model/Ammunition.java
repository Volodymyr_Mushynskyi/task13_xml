package com.epam.model;

public class Ammunition {
  private Integer rockets;
  private Boolean radar;

  public Ammunition() {
  }

  public Ammunition(Integer rockets, Boolean radar) {
    this.rockets = rockets;
    this.radar = radar;
  }

  public Integer getRockets() {
    return rockets;
  }

  public void setRockets(Integer rockets) {
    this.rockets = rockets;
  }

  public Boolean getRadar() {
    return radar;
  }

  public void setRadar(Boolean radar) {
    this.radar = radar;
  }

  @Override
  public String toString() {
    return "Ammunition{" +
            "rockets='" + rockets + '\'' +
            ", radar='" + radar + '\'' +
            '}';
  }
}
