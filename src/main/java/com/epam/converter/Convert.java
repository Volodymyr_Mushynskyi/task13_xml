package com.epam.converter;

import javax.xml.transform.*;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.StringWriter;

public class Convert {

  public static void main(String args[]) {
    Source xml = new StreamSource(new File("src\\main\\resources\\xml\\militaryAircraft.xml"));
    Source xslt = new StreamSource("src\\main\\resources\\xml\\militaryAircraftXSL.xsl");
    convertToHTML(xml, xslt);
  }

  public static void convertToHTML(Source xml, Source xslt) {
    StringWriter sw = new StringWriter();
    try {
      FileWriter file = new FileWriter("src\\main\\resources\\xml\\aircraft.html");
      TransformerFactory tFactory = TransformerFactory.newInstance();
      Transformer transformation = tFactory.newTransformer(xslt);
      transformation.transform(xml, new StreamResult(sw));
      file.write(sw.toString());
      file.close();
    } catch (IOException | TransformerConfigurationException e) {
      e.printStackTrace();
    } catch (TransformerFactoryConfigurationError e) {
      e.printStackTrace();
    } catch (TransformerException e) {
      e.printStackTrace();
    }
  }
}
