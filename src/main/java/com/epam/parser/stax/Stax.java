package com.epam.parser.stax;

import com.epam.model.Ammunition;
import com.epam.model.Chars;
import com.epam.model.Parameters;
import com.epam.model.Plane;

import javax.xml.namespace.QName;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

import static com.epam.constant.Constant.*;

public class Stax {

  private XMLInputFactory xmlInputFactory = XMLInputFactory.newInstance();
  private Plane plane;
  private Chars chars;
  private Ammunition ammunition;
  private Parameters parameters;
  private List<Plane> planeList = new ArrayList<>();

  public List<Plane> parse(File xml) {
    try {
      XMLEventReader xmlEventReader = xmlInputFactory.createXMLEventReader(new FileInputStream(xml));
      while (xmlEventReader.hasNext()) {
        XMLEvent xmlEvent = xmlEventReader.nextEvent();
        if (xmlEvent.isStartElement()) {
          StartElement startElement = xmlEvent.asStartElement();
          String name = startElement.getName().getLocalPart();
          switch (name) {
            case PLANE: {
              plane = new Plane();
              Attribute deviceId = startElement.getAttributeByName(new QName(ID));
              if (deviceId != null) {
                plane.setId(Integer.parseInt(deviceId.getValue()));
              }
              break;
            }
            case MODEL: {
              xmlEvent = xmlEventReader.nextEvent();
              plane.setModel(xmlEvent.asCharacters().getData());
              break;
            }
            case ORIGIN: {
              xmlEvent = xmlEventReader.nextEvent();
              plane.setOrigin(xmlEvent.asCharacters().getData());
              break;
            }
            case CHARS: {
              chars = new Chars();
              break;
            }
            case TYPE: {
              xmlEvent = xmlEventReader.nextEvent();
              chars.setType(xmlEvent.asCharacters().getData());
              break;
            }
            case SEATER: {
              xmlEvent = xmlEventReader.nextEvent();
              chars.setSeater(Integer.parseInt(xmlEvent.asCharacters().getData()));
              break;
            }
            case AMMUNITION: {
              ammunition = new Ammunition();
              break;
            }
            case ROCKETS: {
              xmlEvent = xmlEventReader.nextEvent();
              ammunition.setRockets(Integer.parseInt(xmlEvent.asCharacters().getData()));
              break;
            }
            case RADAR: {
              xmlEvent = xmlEventReader.nextEvent();
              ammunition.setRadar(Boolean.parseBoolean(xmlEvent.asCharacters().getData()));
              break;
            }
            case PARAMETERS: {
              parameters = new Parameters();
              break;
            }
            case LENGTH: {
              xmlEvent = xmlEventReader.nextEvent();
              parameters.setLength(Double.parseDouble(xmlEvent.asCharacters().getData()));
              break;
            }
            case WIDTH: {
              xmlEvent = xmlEventReader.nextEvent();
              parameters.setLength(Double.parseDouble(xmlEvent.asCharacters().getData()));
              break;
            }
            case HEIGHT: {
              xmlEvent = xmlEventReader.nextEvent();
              parameters.setLength(Double.parseDouble(xmlEvent.asCharacters().getData()));
              break;
            }
            case PRICE: {
              xmlEvent = xmlEventReader.nextEvent();
              plane.setPrice(Double.parseDouble(xmlEvent.asCharacters().getData()));
              break;
            }
          }
        }

        if (xmlEvent.isEndElement()) {
          EndElement endElement = xmlEvent.asEndElement();
          String name = endElement.getName().getLocalPart();
          switch (name) {
            case PLANE: {
              planeList.add(plane);
              plane = null;
              break;
            }
            case CHARS: {
              plane.setChars(chars);
              chars = null;
              break;
            }
            case AMMUNITION: {
              chars.setAmmunition(ammunition);
              ammunition = null;
              break;
            }
            case PARAMETERS: {
              plane.setParameters(parameters);
              parameters = null;
              break;
            }
          }
        }
      }
    } catch (XMLStreamException | FileNotFoundException e) {
      e.printStackTrace();
    }
    return planeList;
  }
}
