package com.epam.parser.stax;

import com.epam.model.Plane;

import java.io.File;
import java.util.List;

public class Starter {

  public static void main(String[] args) {
    File xmlFile = new File("src\\main\\resources\\xml\\militaryAircraft.xml");
    File xsdFile = new File("src\\main\\resources\\xml\\militaryAircraftXSD.xsd");
    Stax stax = new Stax();
    List<Plane> planes = stax.parse(xmlFile);
    planes.stream()
            .forEach(System.out::println);
  }
}
