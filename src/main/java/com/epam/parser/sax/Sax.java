package com.epam.parser.sax;

import com.epam.model.Plane;
import com.epam.xmlValidator.XmlValidator;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Sax {
  private static SAXParserFactory saxParserFactory = SAXParserFactory.newInstance();

  public static List<Plane> parse(File xml, File xsd) {
    List<Plane> planes = new ArrayList<>();
    try {
      saxParserFactory.setSchema(XmlValidator.createSchema(xsd));
      saxParserFactory.setValidating(true);
      SAXParser saxParser = saxParserFactory.newSAXParser();
      System.out.println(saxParser.isValidating());
      Handler handler = new Handler();
      saxParser.parse(xml, handler);
      planes = handler.getPlaneList();
    } catch (ParserConfigurationException e) {
      e.printStackTrace();
    } catch (IOException e) {
      e.printStackTrace();
    } catch (SAXException e) {
      e.printStackTrace();
    }
    return planes;
  }
}
