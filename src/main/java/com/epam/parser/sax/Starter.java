package com.epam.parser.sax;

import com.epam.compare.Compare;
import com.epam.model.Plane;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.util.Collections;
import java.util.List;

public class Starter {
  private static Logger logger = LogManager.getLogger(Starter.class);

  public static void main(String[] args) {
    File xmlFile = new File("src\\main\\resources\\xml\\militaryAircraft.xml");
    File xsdFile = new File("src\\main\\resources\\xml\\militaryAircraftXSD.xsd");

    List<Plane> planeList = Sax.parse(xmlFile, xsdFile);
    planeList.stream()
            .forEach(System.out::println);
    logger.info("Sorting element");
    Collections.sort(planeList, new Compare());
    planeList.stream()
            .forEach(System.out::println);
  }
}
