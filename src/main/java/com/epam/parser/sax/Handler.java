package com.epam.parser.sax;

import com.epam.model.Ammunition;
import com.epam.model.Chars;
import com.epam.model.Parameters;
import com.epam.model.Plane;
import com.epam.parser.sax.menu.Endble;
import com.epam.parser.sax.menu.Startble;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import static com.epam.constant.Constant.*;

public class Handler extends DefaultHandler {

  private Plane plane;
  private Chars chars;
  private Ammunition ammunition;
  private Parameters parameters;
  private String element;
  private List<Plane> planeList = new ArrayList<Plane>();
  private Map<String, Startble> methodsStartElement;
  private Map<String, Endble> methodsEndElement;

public Handler(){
  createMethodsStartElement();
  createMethodsEndElement();
}

  public List<Plane> getPlaneList() {
    return this.planeList;
  }

  private void createMethodsStartElement() {
    methodsStartElement = new LinkedHashMap<>();
    methodsStartElement.put(PLANE, this::planeInitialize);
    methodsStartElement.put(CHARS, this::charsInitialize);
    methodsStartElement.put(PARAMETERS, this::parametersInitialize);
  }

  private void createMethodsEndElement() {
    methodsEndElement = new LinkedHashMap<>();
    methodsEndElement.put(PLANE, this::addPlane);
    methodsEndElement.put(CHARS, this::setCharsAndAmmunition);
    methodsEndElement.put(PARAMETERS, this::setParameters);
  }

  @Override
  public void startElement(String uri, String localName, String current, Attributes attributes) throws SAXException {
    element = current;

    if (methodsStartElement.containsKey(element)) {
      methodsStartElement.get(element).printStartble(attributes);
    }
  }

  private void parametersInitialize(Attributes attributes) {
    parameters = new Parameters();
  }

  private void charsInitialize(Attributes attributes) {
    chars = new Chars();
    ammunition = new Ammunition();
  }


  private void planeInitialize(Attributes attributes) {
    String deviceId = attributes.getValue(ID);
    plane = new Plane();
    plane.setId(Integer.parseInt(deviceId));
  }

  @Override
  public void endElement(String uri, String localName, String currentName) throws SAXException {

    if (methodsEndElement.containsKey(currentName)) {
      methodsEndElement.get(currentName).printEndble();
    }
  }

  private void setParameters() {
    plane.setParameters(parameters);
    parameters = null;
  }

  private void setCharsAndAmmunition() {
    plane.setChars(chars);
    chars.setAmmunition(ammunition);
    ammunition = null;
    chars = null;
  }

  private void addPlane() {
    planeList.add(plane);
  }

  @Override
  public void characters(char[] symbol, int start, int length) {
    if (element.equals(MODEL)) {
      plane.setModel(new String(symbol, start, length));
    }
    if (element.equals(ORIGIN)) {
      plane.setOrigin(new String(symbol, start, length));
    }
    if (element.equals(TYPE)) {
      chars.setType(new String(symbol, start, length));
    }
    if (element.equals(SEATER)) {
      chars.setSeater(Integer.parseInt(new String(symbol, start, length)));
    }
    if (element.equals(ROCKETS)) {
      ammunition.setRockets(Integer.parseInt(new String(symbol, start, length)));
    }
    if (element.equals(RADAR)) {
      ammunition.setRadar(Boolean.parseBoolean(new String(symbol, start, length)));
    }
    if (element.equals(LENGTH)) {
      parameters.setLength(Double.parseDouble(new String(symbol, start, length)));
    }
    if (element.equals(WIDTH)) {
      parameters.setWidth(Double.parseDouble(new String(symbol, start, length)));
    }
    if (element.equals(HEIGHT)) {
      parameters.setHeight(Double.parseDouble(new String(symbol, start, length)));
    }
    if (element.equals(PRICE)) {
      plane.setPrice(Double.parseDouble(new String(symbol, start, length)));
    }
  }
}
