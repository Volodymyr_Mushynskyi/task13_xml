package com.epam.xmlValidator;

import javax.xml.XMLConstants;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import java.io.File;

public class XmlValidator {

  public static Schema createSchema(File xsd) {
    Schema schema = null;
    try {
      String language = XMLConstants.W3C_XML_SCHEMA_NS_URI;
      SchemaFactory factory = SchemaFactory.newInstance(language);
      schema = factory.newSchema(xsd);
    } catch (Exception e) {
      e.printStackTrace();
    }
    return schema;
  }
}
